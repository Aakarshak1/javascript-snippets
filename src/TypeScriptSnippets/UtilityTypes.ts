type User = {
  id: number;
  name?: string;
  email?: string;
  age?: number;
};

type PartialUser = Partial<User>;

type RequiredUser = Required<User>;

type ReadonlyUser = Readonly<User>;

type PickUser = Pick<User, 'id' | 'name'>;

type OmitUser = Omit<User, 'id'>;

type PartialPickUser = Partial<PickUser>;

type Mutable<T> = {
  -readonly [K in keyof T]: T[K];
};

type MutableUser = Mutable<User>;

type Role = 'admin' | 'user' | 'super-admin' | 'guest';

type NonAdminRole = Exclude<Role, 'admin' | 'super-admin'>;

type RoleAttributes = { role: 'admin'; orgId: string } | { role: 'user' } | { role: 'guest' };

type AdminRole = Extract<RoleAttributes, { role: 'admin' }>;

type Func = (a: number, b: string) => number;
type ReturnValue = ReturnType<Func>;
type Params = Parameters<Func>;

type MaybeString = string | null | undefined;

type DefinitelyString = NonNullable<MaybeString>;

type PromiseString = Promise<String>;

type Result = Awaited<PromiseString>;

const func = async () => {
  return {
    id: 123,
  };
};

type funcResultType = Awaited<ReturnType<typeof func>>;

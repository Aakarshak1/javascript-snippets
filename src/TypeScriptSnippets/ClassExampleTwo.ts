interface ITakePhoto {
  cameraMode: string;
  filter: string;
  burst: number;
}

class InstagramClass implements ITakePhoto {
  constructor(public cameraMode: string, public filter: string, public burst: number) {}
}

class YoutubeClass implements ITakePhoto {
  constructor(
    public cameraMode: string,
    public filter: string,
    public burst: number,
    public shots: number
  ) {}
}

/// Abstract Class  -- no new object can be created from it

abstract class TakePhotoAbstractExample {
  constructor(public cameraMode: string, public filter: string, public burst: number) {}

  abstract getAbstractMethod(): void;
}

class InstagramAbstract extends TakePhotoAbstractExample {
  constructor(public cameraMode: string, public filter: string, public burst: number) {
    super(cameraMode, filter, burst);
  }

  getAbstractMethod(): void {
    console.log('Implementing Abstract method from InstagramAbstract class');
  }
}

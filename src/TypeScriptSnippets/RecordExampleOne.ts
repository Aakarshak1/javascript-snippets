type Fruit = 'apple' | 'banana' | 'orange';
type Price = number;

const fruitPrices: Record<Fruit, Price> = {
  apple: 0.5,
  banana: 0.25,
  orange: 0.75,
};

console.log(fruitPrices.apple); // Output: 0.5
console.log(fruitPrices.banana); // Output: 0.25
console.log(fruitPrices.orange); // Output: 0.75

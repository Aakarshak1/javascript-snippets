class User {
  public email: string;
  name: string;
  city: string = 'Bangalore';
  private readonly country: string = 'India';
  private _accountBalance: number = 10000;

  constructor(email: string, name: string) {
    this.email = email;
    this.name = name;
  }

  get getEmail(): string {
    return this.email;
  }

  set setEmail(email: string) {
    this.email = email;
  }

  public get accountBalance(): number {
    return this._accountBalance;
  }

  public set accountBalance(balance: number) {
    this._accountBalance = balance;
  }

  //accessible from class only
  private privateMethod(): void {
    console.log('this is private method');
  }

  protected protectedVariable: number = 1;
}

class SubUser extends User {
  isFamily: boolean = true;

  constructor(email: string, name: string) {
    super(email, name);
  }

  changeProtectedVariable(): void {
    this.protectedVariable = 2;
  }
}

const user = new SubUser('John', 'Kumar@gmail.com');

const userTwo = new User('Mary', 'sw@you.com');

console.log(userTwo.city);

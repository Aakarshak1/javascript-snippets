//type
type User = {
  readonly _id: string;
  name: string;
  age: number;
  isActive: boolean;
};

const arr: number[] = [];

const multiArr: number[][] = [];

//union types
let score: number | string = 38;
score = '38';

function getUser(id: string | number): string {
  //if we want to perform any string related operation to id, first we need to perform check on id type
  if (typeof id === 'string') {
    return id.toLowerCase();
  }

  return id.toString();
}

//array with union
const mixArr: (string | number | boolean)[] = [1, 2, '3', true];

//we can set constant value also in type
const PI: 3.14 = 3.14;

let seatAllotment: 'aisle' | 'middle' | 'window';

seatAllotment = 'middle';
// seatAllotment='none'  // will throw error since its not assignable to type or not defined in type

//Tuples
//tuple is a fixed length array
// order of item inside the defined array should be same as type defined in the array
let employee: [string, number] = ['John', 38];
const RGB: [number, number, number] = [255, 234, 21];

//ENUMS
enum SeatChoices {
  AISLE = 'aisle',
  MIDDLE = 'middle',
  WINDOW = 'window',
}

const seat = SeatChoices.AISLE;

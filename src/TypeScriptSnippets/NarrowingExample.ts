//Narrowing

interface User {
  name: string;
  email: string;
}

interface Admin {
  name: string;
  email: string;
  isAdmin: boolean;
}

function isAdmin(account: User | Admin): void {
  if ('isAdmin' in account) {
    console.log(account.isAdmin);
  }
}

isAdmin({ name: 'John', email: '0fqrq@example.com', isAdmin: true });
isAdmin({ name: 'John', email: '0fqrq@example.com', isAdmin: false });

//Discriminated Union
interface Circle {
  kind: 'circle';
  radius: number;
}

interface Square {
  kind: 'square';
  sideLength: number;
}

interface Rectangle {
  kind: 'rectangle';
  width: number;
  height: number;
}

type Shape = Circle | Square | Rectangle;

function getArea(shape: Shape) {
  switch (shape.kind) {
    case 'circle':
      return Math.PI * shape.radius ** 2;
    case 'square':
      return shape.sideLength ** 2;
    case 'rectangle':
      return shape.width * shape.height;
    default:
      const _defaultForShape: never = shape;
      return _defaultForShape;
  }
}

// using instanceof
function logValue(value: Date | string) {
  if (value instanceof Date) console.log('Date');
  else console.log('String');
}

logValue(new Date());
logValue('hello');

//using type predicate
type Fish = { swim: () => void };
type Bird = { fly: () => void };

function isFish(pet: Fish | Bird): pet is Fish {
  return (pet as Fish).swim !== undefined;
}

function getFood(pet: Fish | Bird) {
  if (isFish(pet)) {
    pet;
    return 'fish food';
  } else {
    pet;
    return 'bird food';
  }
}

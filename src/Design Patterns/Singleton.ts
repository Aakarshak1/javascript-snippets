// Only one instance of object at a time

const Singleton = (function () {
  let instance: any;

  function createInstance() {
    // const object = new Object({name : 'John Doe'})
    // return object;
    return new Object({ name: 'John Doe' });
  }

  return {
    getInstance: () => {
      if (!instance) instance = createInstance();
      return instance;
    },
  };
})();

const instanceA = Singleton.getInstance();
const instanceB = Singleton.getInstance();

console.log(instanceA);
console.log(instanceA === instanceB);

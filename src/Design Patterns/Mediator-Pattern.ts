/**
 * The Mediator pattern provides central authority over a group of objects by encapsulating how these objects interact.
 * This model is useful for scenarios where there is a need to manage complex conditions in which every object is
 * aware of any state change in any other object in the group.
 *
 * it provide interface between mediator
 */

// TODO code cleanup and add correct type

class User {
  name: string;
  chatroom?: any;
  constructor(name: string) {
    this.name = name;
  }

  send(message: string, to: User | undefined = undefined) {
    this.chatroom.send(message, this, to);
  }

  receive(message: string, from: any) {
    console.log(`${from.name} to ${this.name} : ${message}`);
  }
}

class Chatroom {
  users: any;

  constructor() {
    this.users = [];
  }

  register(user: User) {
    console.log(user);
    this.users[user.name] = user;
    user.chatroom = this;
  }

  send(message: string, from: string, to: User | undefined = undefined) {
    if (to) {
      to.receive(message, from);
    } else {
      for (let key in this.users) {
        if (this.users[key] !== from) {
          this.users[key].receive(message, from);
        }
      }
    }
  }
}

const brad = new User('Brad');
const jeff = new User('Jeff');
const sara = new User('Sara');

const chatroom = new Chatroom();

chatroom.register(brad);
chatroom.register(jeff);
chatroom.register(sara);

brad.send('Hello Jeff', jeff);
sara.send('Hello Brad, you are the best dev ever!', brad);
jeff.send('Hello Everyone!!!!');

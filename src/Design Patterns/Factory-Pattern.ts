// way of creating interface for objects in JS
//generally used to maintain  and manipulate a collective set that are different but same time have many common characteristic

class MemberFactory {
  createMember(name: string, type: 'basic' | 'standard' | 'elite') {
    let member: {
      name?: string;
      type?: string;
      define?: () => void;
    } = {};

    switch (type) {
      case 'basic':
        member = new SimpleMembership(name);
        break;
      case 'standard':
        member = new StandardMembership(name);
        break;
      case 'elite':
        member = new EliteMembership(name);
        break;
    }

    member.type = type;

    member.define = function (this: any) {
      console.log(`${this.name} ==> ${this.type}: ${this.cost}`);
    };

    return member;
  }
}

class SimpleMembership {
  name: string;
  cost: string;

  constructor(name: string) {
    this.name = name;
    this.cost = '15';
  }
}

class StandardMembership {
  name: string;
  cost: string;

  constructor(name: string) {
    this.name = name;
    this.cost = '25';
  }
}

class EliteMembership {
  name: string;
  cost: string;

  constructor(name: string) {
    this.name = name;
    this.cost = '25';
  }
}

const members = [];

const factory = new MemberFactory();

members.push(factory.createMember('john Doe', 'basic'));
members.push(factory.createMember('Chris Jackson', 'standard'));
members.push(factory.createMember('Tom Smith', 'elite'));

console.log(members);

members.forEach((member: any) => {
  member.define();
});

/**
function MemberFactory() {
  this.createMember = function(name, type) {
    let member;

    if (type === "basic") {
      member = new SimpleMembership(name);
    } else if (type === "standard") {
      member = new StandardMembership(name);
    } else if (type === "Elite") {
      member = new EliteMemberShip(name);
    }

    member.type = type;

    member.define = function() {
      console.log(`${this.name} ==> ${this.type}: ${this.cost}`);
    };

    return member;
  };
}

const SimpleMembership = function(name) {
  this.name = name;
  this.cost = "15";
};

const StandardMembership = function(name) {
  this.name = name;
  this.cost = "25";
};

const EliteMemberShip = function(name) {
  this.name = name;
  this.cost = "55";
};

const members = [];

const factory = new MemberFactory();

members.push(factory.createMember("john Doe", "basic"));
members.push(factory.createMember("Chris Jackson", "standard"));
members.push(factory.createMember("Tom Smith", "Elite"));

console.log(members);

members.forEach(member => {
  member.define();
});

 * 
 */

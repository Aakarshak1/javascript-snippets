class Queue {
  private elements: number[] = [];
  constructor() {}
  enqueue(element: number): void {
    this.elements.push(element);
  }

  dequeue(): number | undefined {
    return this.elements.shift();
  }

  peek(): number | undefined {
    if (this.isEmpty()) return undefined;
    else return this.elements[0];
  }

  isEmpty(): boolean {
    return this.elements.length === 0;
  }

  clear(): void {
    this.elements = [];
  }

  size(): number {
    return this.elements.length;
  }

  //Remove and return the first element
  poll(): number | undefined {
    if (this.isEmpty()) return undefined;
    else return this.elements.shift();
  }
}

export default Queue;

let queue = new Queue();

queue.enqueue(1);
queue.enqueue(2);
queue.enqueue(8);
queue.dequeue();

console.log(queue);

// Array.from() returns new array from an array-like or iterable object

console.log(Array.from<string>('foo')); // ['f', 'o', 'o']

// it accepts on optional parameter mapFn which allows to execute map function
const squareFn = (x: number): number => x * x;
console.log(Array.from<number, number>([1, 2, 3], squareFn));

const map: Map<number, number> = new Map([
  [1, 2],
  [2, 4],
  [4, 8],
]);

Array.from<[number, number]>(map);
// [[1, 2], [2, 4], [4, 8]]

const mapper: Map<string, string> = new Map([
  ['1', 'a'],
  ['2', 'b'],
]);
Array.from<string>(mapper.values());
// ['a', 'b'];

Array.from<string>(mapper.keys());
// ['1', '2'];

const obj = {
  name: 'test',
  address: {
    personal: 'abc',
    office: {
      building: 'random',
      street: 'some street',
    },
  },
};

const flattenObjectOne = (obj, res = {}, prefix = undefined) => {
  for (let key in obj) {
    const propKey = prefix ? `${prefix}_${key}` : key;
    if (typeof obj[key] === 'object') flattenObjectOne(obj[key], res); // try with passing propKey
    else res[propKey] = obj[key];
  }

  return res;
};

const flatObjectTwo = (obj, parentKey = undefined) => {
  const result = {};
  Object.keys(obj).forEach((key) => {
    const value = obj[key];
    const _key = parentKey ? `${parentKey}_${key}` : key;
    if (typeof value === 'object') {
      result = { ...result, ...flatObjectTwo(value, _key) };
    } else {
      result[_key] = value;
    }
  });

  //using entries
  Object.entries(obj).forEach(([key, value]) => {
    const _key = parentKey ? parentKey + '.' + key : key;
    if (typeof value === 'object') {
      result = { ...result, ...flattenObject(value, _key) };
    } else {
      result[_key] = value;
    }
  });

  return result;
};

const result = flattenObjectOne(obj);
console.log(result);

/**
  Debounce is ideal solution for things where we want to delay some action call by a set period of time 
  If nothing happens during that time then the function will run just like normal, but if something happens 
  that causes the function to be called again during the delay then the delay will be restarted. 
  This means a debounced function will only run once after a certain delay since the last time it was triggered.

  basic logic 
  1. Initialize a timer variable that controls when to run a callback function
  2. Reset the timer function every time the user starts an action
  */

// in most of cases below will be enough
const debounceOne = (func: Function, delay: number): Function => {
  /**
   * In a Web Browser app, setTimeout returns a number.
   * In a NodeJS app, setTimeout returns a NodeJS.Timeout.
   *
   *  we can use ReturnType generic with return type of setTimeout to make portable
   *  between node.js and browser environment.
   */
  let debounceTimer: ReturnType<typeof setTimeout>;

  //track whether the debounced function has been called before or not
  let hasBeenCalled: boolean = true;

  return function (this: Function) {
    const context = this;
    const args = arguments;
    if (hasBeenCalled) {
      //debounced function has been called at least once before within the specified delay.
      clearTimeout(debounceTimer);
      debounceTimer = setTimeout(() => func.apply(context, args), delay);
    } else {
      //debounced function is being called for the first time or after the specified delay has passed.
      hasBeenCalled = true;
      debounceTimer = setTimeout(() => func.apply(context, args), delay);
    }
  };
};

const debounceTwo = (func: Function, wait: number, immediate: boolean): Function => {
  let debounceTimer: ReturnType<typeof setTimeout> | undefined;

  return function executedFunction(this: Function) {
    let context = this;
    const args = arguments;

    // execute after some time
    const laterFunction = function () {
      debounceTimer = undefined;
      if (!immediate) func.apply(context, args);
    };

    const callNow = immediate && !debounceTimer;

    clearTimeout(debounceTimer);
    debounceTimer = setTimeout(laterFunction, wait);

    if (callNow) func.apply(context, args);
  };
};

const debounceThree = (func: Function, wait: number) => {
  let debounceTimer: ReturnType<typeof setTimeout> | undefined;

  const debouncedFunc = (args: any[]) =>
    new Promise((resolve) => {
      if (debounceTimer) clearTimeout(debounceTimer);

      debounceTimer = setTimeout(() => {
        resolve(func(args));
      }, wait);
    });

  const teardown = () => clearTimeout(debounceTimer);

  return [debouncedFunc, teardown];
};

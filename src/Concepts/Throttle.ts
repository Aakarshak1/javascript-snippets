/**
 * https://codesandbox.io/s/laughing-matsumoto-3csp4q?file=%2Fsrc%2FApp.js
 * throttle test with react
 *
 */

/**
    Throttling implies limiting the number of times a function gets called in a certain time period. 
    It will prohibit a function from executing if we have invoked it “recently.” Throttling also guarantees 
    that a function runs at a consistent rate.

     if our delay is set to 1 second then our throttled function will execute immediately when it is called and then at most once per second while the user is actively typing
 
     */

const throttleOne = (func: Function, delay: number): Function => {
  let last: number;

  let timeoutID: ReturnType<typeof setTimeout> | undefined;

  /**
   * The throttling behavior is achieved by tracking the last time the throttled function
   * was called and using setTimeout to delay subsequent calls. If the time difference
   * between the current call and the last call is greater than or equal to the specified
   * delay time, the func function is immediately called. Otherwise, the func function is
   * called after the remaining time is waited. The throttled function also preserves
   * the context (this) and arguments of the original function when calling it.
   */

  return function throttled(this: Function) {
    let ctx = this;
    const args = arguments;
    let delta: number = +new Date() - last;
    if (!timeoutID)
      if (delta >= delay) call();
      else timeoutID = setTimeout(call, delay - delta);
    return func.apply(ctx, args);
  };

  function call() {
    timeoutID = undefined;
    last = +new Date();
  }
};

function resize(evt: any) {
  console.log('height', window.innerHeight);
  console.log('width', window.innerWidth);
}

window.addEventListener('resize', (e) => throttleOne(resize, 200));

// use this one most of time
const throttleTwo = (func: Function, delay: number): Function => {
  let throttlePause: boolean = false;
  //queue up any action that occurs in your throttle so that as soon as your delay is over it will call the previous iteration of the function
  //The waitingArgs variable is used to store the arguments passed to the throttled function while it is waiting for the delay to expire.

  let waitingArgs: any;

  /*
   *  here when our delay ends we check to see if we have any waitingArgs.
   *  If we do not we just do everything as normal and set shouldWait to false so we can wait for the next trigger.
   *  If we do have waitingArgs, though, that means we called throttle during the delay and we want to trigger our
   *  function with those waitingArgs and then reset our timer.
   */
  const calledFunction = () => {
    if (waitingArgs == null) {
      throttlePause = false;
    } else {
      func(...waitingArgs);
      waitingArgs = null;
      setTimeout(calledFunction, delay);
    }
  };

  return (...args: any[]) => {
    // don't run the function if throttlePause is true
    if (throttlePause) {
      /*
       * When the throttled function is called multiple times within the delay period,
       * the previous arguments are overwritten by the latest ones. This ensures that the
       * function is eventually executed with the latest arguments that were passed to it.
       */
      waitingArgs = args;
      return;
    }

    func(...args);

    // set throttlePause to true after the if condition , allowing function to run once
    throttlePause = true;

    // setTimeout runs the function within specific time
    setTimeout(calledFunction, delay);
  };
};

const throttleThree = (func: Function, delay: number, immediate: boolean = false): Function => {
  let throttleTimer: ReturnType<typeof setTimeout> | undefined;
  let initialCall: boolean = true;

  return function (this: Function) {
    const callNow: boolean = immediate && initialCall;
    const next = () => {
      throttleTimer = undefined;
      func.apply(this, arguments);
    };

    if (callNow) {
      initialCall = false;
      next();
    }

    if (!throttleTimer) {
      throttleTimer = setTimeout(next, delay);
    }
  };
};

/**
 *  Object.freeze() is used to create immutable object , it freezes an Object
 *
 * Freezing an object does not allow new properties to be added to an object and
 * prevents from removing or altering the existing properties
 *
 * A frozen object can no longer be changed; freezing an object prevents new
 * properties from being added to it, existing properties from being removed,
 * prevents changing the enumerability, configurability, or write-ability of existing
 * properties, and prevents the values of existing properties from being changed.
 * In addition, freezing an object also prevents its prototype from being changed.
 *
 * Object.freeze() returns the object that was passed to the function.
 *
 */
//creating immutable hobbies array
const hobbies = Object.freeze(['programming', 'reading', 'music']);

//splice method  changes the contents of an array by removing or replacing existing elements and/or adding new elements in place.
const firstTwo = hobbies.splice(0, 2); // throw error since we are trying to mutate our immutable array

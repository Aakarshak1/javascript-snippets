function* getNumber(): Generator<number> {
  yield 42;
  yield 43;
  yield 44;
}

let iterator = getNumber();

console.log(iterator.next().value);
console.log(iterator.next().value);
console.log(iterator.next().value);
console.log(iterator.next().value);

// Generator with object
const someObj = {
  *generator() {
    yield 'a';
    yield 'b';
  },
};

//as an Object property
const gen = someObj.generator();

console.log(gen.next());
console.log(gen.next());
console.log(gen.next());

// Generator Yield with a Function Call
function* fetchUser() {
  const user: object = yield getData();
  console.log(user);
}

function getData() {
  return {
    name: 'Ram',
    age: 24,
  };
}

let fetchGen = fetchUser();

console.log(fetchGen.next().value);
console.log(fetchGen.next());

// yield from another generator function
function* generator1() {
  yield 2;
  yield 3;
  yield 4;
}

function* generator2() {
  yield 1;
  yield* generator1();
  yield 5;
}

let iterator_one = generator2();

console.log(iterator_one.next());
console.log(iterator_one.next());
console.log(iterator_one.next());
console.log(iterator_one.next());
console.log(iterator_one.next());
console.log(iterator_one.next());

//yield* with return
function* getFunChild() {
  yield 1;
  yield 2;
  return 'foo';
  yield 3; //this value will be ignored since above we use return
}

function* getFuncMain() {
  const result = yield* getFunChild();
  console.log(result);
  yield 'the end';
}

let Generator = getFuncMain();

console.log(Generator.next());
console.log(Generator.next());
console.log(Generator.next());
console.log(Generator.next());

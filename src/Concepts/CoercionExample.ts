// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore
console.log(((2.0 == '2') == new Boolean(true)) == '1');

/**
 *  What will be the output above ?
 *
 *  1. Divide it in smaller part
 *      a = 2.0
 *      b = '2'
 *      c = new Boolean(true)
 *      d = '1'
 *
 *
 *  2. Since b is string it will be converted to Number, then a==b will return true (which will become e, e = true)
 *  3. e is value but c is object, by implicit conversion c will return true, e == c == true (which will become f, f = true)
 *  4. f will be converted to number and, 1 == '1', d will get converted to number 1
 *  5. 1 == 1, it will log 'true' in console
 */

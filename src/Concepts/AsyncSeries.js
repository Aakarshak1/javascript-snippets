/**
 * Run the functions in the tasks collection in series, each one running once the previous function has completed. If any functions in the series pass an error to its callback, no more functions are run, and callback is immediately called with the value of the error
 *
 */
function createAsyncTask() {
  const value = Math.floor(Math.random() * 10);
  return function (callback) {
    setTimeout(() => {
      callback(value);
    }, 1000);
  };
}

// Array.reduce since we care about the order of result
//As we have to execute the tasks sequentially, we will use Promise,
//the starting value of of the reduce array will be Promise.resolve()

function asyncSeries(taskList, callback) {
  let results = [];
  let tasksCompleted = 0;

  taskList.reduce((accumulator, current) => {
    return accumulator.then((someVal) => {
      return new Promise((resolve, reject) => {
        current((value) => {
          results.push(value);
          tasksCompleted++;

          if (tasksCompleted === taskList.length) {
            callback.call(null, results);
          } else {
            resolve(value);
          }
        });
      });
    });
  }, Promise.resolve());
}

const taskList = [
  createAsyncTask(),
  createAsyncTask(),
  createAsyncTask(),
  createAsyncTask(),
  createAsyncTask(),
];
asyncSeries(taskList, (result) => {
  console.log('got the results', result);
});

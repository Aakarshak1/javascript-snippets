const arr = [[[[[0]], [1]], [[[2], [3]]], [[4], [5]]]];

const flattenArrayOne = (arr) => {
  let resultedArray = [];

  for (let i = 0; i < arr.length; i++) {
    if (Array.isArray(arr[i])) resultedArray = resultedArray.concat(flattenArrayOne(arr[i]));
    else resultedArray.push(arr[i]);
  }
  return resultedArray;
};

const flattenArrayTwo = (arr) => {
  let resultedArray = [];

  for (let i = 0; i < arr.length; i++) {
    if (Array.isArray(arr[i])) {
      resultedArray.push(...flattenArrayTwo(arr[i]));
    } else {
      resultedArray.push(arr[i]);
    }
  }
  return resultedArray;
};

const flattenArrayThree = (arr) => {
  if (!Array.isArray(arr)) return arr ? [arr] : [];

  return arr.reduce((acc, item) => {
    if (Array.isArray(item)) {
      return [...acc, ...flattenArrayThree(item)];
    }
    return [...acc, item];
  }, []);
};

Array.prototype.myFlat = function () {
  const output = [];
  function flattenArray(arr) {
    for (let i = 0; i < arr.length; i++) {
      if (Array.isArray(arr[i])) {
        flattenArray(arr[i]);
      } else {
        output.push(arr[i]);
      }
    }
    return output;
  }
  const returnValue = flattenArray(this);
  return returnValue;
};

// const resultOne = flattenArrayOne(arr);
// const resultTwo = flattenArrayTwo(arr);
// const resultThree = flattenArrayThree(arr);
// console.log({ resultOne });
// console.log({ resultTwo });
// console.log({ resultThree });
// console.log(arr.myFlat());

// n == depth for flattening array
var flat = function (arr, n) {
  if (n <= 0) return arr;

  let resultedArr = [];

  for (let i = 0; i < arr.length; i++) {
    if (Array.isArray(arr[i])) resultedArr = resultedArr.concat(flat(arr[i], n - 1));
    else resultedArr.push(arr[i]);
  }

  return resultedArr;
};

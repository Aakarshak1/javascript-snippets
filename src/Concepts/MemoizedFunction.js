/*
  slowFunc(params) // slow output
  memoizedFunc = memoize(slowFunc)
  memoizedFunc(params) // slow output
  memoizedFunc(params) // fast output
  memoizedFunc(params) // fast output 
 */
// Basic logic is to create High Order Function which will return function

function memoize(fn) {
  const cachedValue = {};
  return function () {
    const args = JSON.stringify(arguments);
    if (cachedValue[args]) return cachedValue[args];

    const value = fn.apply(this, arguments);
    cachedValue[args] = value;

    return value;
  };
}

function factorial(n) {
  if (n === 0 || n === 1) {
    return 1;
  }
  return factorial(n - 1) * n;
}

const memoizedFactorial = memoize(factorial);
memoizedFactorial(1000); // slow
memoizedFactorial(1000); // faster

/***********************************************************/
/***********************************************************/
/***********************************************************/

const addFunction = (n) => n + 10;
const memoizedAdd = () => {
  let cache = {};
  return (n) => {
    if (n in cache) {
      return cache[n];
    } else {
      let result = n + 10;
      cache[n] = result;
      return result;
    }
  };
};

const newAdd = memoizedAdd();
console.log(newAdd(9)); // calculated
console.log(newAdd(9)); // cached

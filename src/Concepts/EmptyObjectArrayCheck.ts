// www.samanthaming.com/tidbits/89-how-to-check-if-variable-is-array/
//https://www.samanthaming.com/tidbits/94-how-to-check-if-object-is-empty/
//www.scaler.com/topics/check-if-object-is-empty-javascript/
//https://builtin.com/software-engineering-perspectives/javascript-check-if-object-is-empty
//https://www.freecodecamp.org/news/check-if-an-object-is-empty-in-javascript/

const emptyObject = {};

let isEmpty;

// method 1
// Constructor check to cover wrapper instances
isEmpty = Object.keys(emptyObject).length === 0 && emptyObject.constructor === Object;

// method 2

// check for null and undefined
isEmpty =
  emptyObject && Object.keys(emptyObject).length === 0 && emptyObject.constructor === Object;

// method 3
function isEmptyObjectCheckOne(object: Object) {
  for (var key in object) {
    if (object.hasOwnProperty(key)) {
      return false;
    }
  }

  return true;
}

function isEmptyObjectTwo(value: Object) {
  if (value == null) {
    // null or undefined
    return false;
  }

  if (typeof value !== 'object') {
    // boolean, number, string, function, etc.
    return false;
  }

  const proto = Object.getPrototypeOf(value);

  // consider `Object.create(null)`, commonly used as a safe map
  // before `Map` support, an empty object as well as `{}`
  if (proto !== null && proto !== Object.prototype) {
    return false;
  }

  return isEmptyObjectCheckOne(value);
}

isEmptyObjectCheckOne(emptyObject);

const emptyArray: any[] = [];

// method 1
isEmpty = Array.isArray(emptyArray);

//Promise Example1

const OnlineDelivery = new Promise<string>((resolve, reject) => {
  setTimeout(() => {
    resolve('Order Delivered');
  }, 100000);

  reject('Order Not Delivered');
});

const cashOnDelivery = new Promise<Record<string, number | string>>((resolve, reject) => {
  setTimeout(() => {
    resolve({
      amount: 100,
      note: 'Thank you',
    });
  }, 100000);

  reject({
    amount: 0,
    note: 'Product is defective',
  });
});

// Promise Example2

const getRandomInt = (): string => {
  return Math.floor(Math.random() * 10).toString();
};

// resolve with random number
const findEven = new Promise<number>((resolve, reject) => {
  setTimeout((): void => {
    const value: number = parseInt(getRandomInt());
    // const value = getRandomInt();  // will throw error if value is not a number

    if (value % 2 === 0) {
      resolve(value);
    } else {
      reject('Odd Number found!');
    }
  }, 1000);
});

findEven.then((value) => console.log(value)).catch((error) => console.log(error));

// promise resolve
const value: number = parseInt(getRandomInt());
const numPromise = Promise.resolve(value);
numPromise.then((value) => console.log(value)).catch((error) => console.log(error));

// promise reject
const errMsg: string = 'Oops! Something went wrong';
const errPromise = Promise.reject(errMsg);
errPromise.then((value) => console.log(value)).catch((error) => console.log(error));

//Promise.all

const getPromise = (value: number, delay: number, fail: boolean): Promise<number> => {
  return new Promise<number>((resolve, reject) => {
    setTimeout(() => (fail ? reject(value) : resolve(value)), delay);
  });
};

// async await
interface Employee {
  id: number;
  employee_name: string;
  employee_salary: number;
  employee_age: number;
  profile_image: string;
}

//Utility function
function getErrorMessage(error: unknown) {
  if (error instanceof Error) return error.message;
  return String(error);
}

const fetchEmployees = async (): Promise<Array<Employee> | string | undefined> => {
  const api = 'http://dummy.restapiexample.com/api/v1/employees';
  try {
    const response = await fetch(api);
    const { data } = await response.json();
    return data;
  } catch (error) {
    if (error) {
      let message;
      if (error instanceof Error) message = error.message;
      else message = String(error);

      reportError({ message });
    }
  }
};

const fetchAllEmployees = async (url: string): Promise<Employee[]> => {
  const response = await fetch(url);
  const { data } = await response.json();
  return data;
};

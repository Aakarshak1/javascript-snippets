/*  1. Don't use Arrow Function to define Object Literal */

/**
 *  When invoking the method sum() on the below object, the context remains window.
 *  because the arrow function binds the context lexically with the window object
 */
const calculate_withArrow = {
  array: [1, 2, 3],
  sum: () => {
    console.log(this === window); // => true
    return this.array.reduce((result, item) => result + item);
  },
};
calculate_withArrow.sum(); // Throws "TypeError: Cannot read property 'reduce' of undefined"

/**
 *  The solution is to use a function expression or shorthand syntax for method definition
 *  here the context for this is determined by the invocation not by the enclosing context
 */

const calculate = {
  array: [1, 2, 3],
  sum() {
    console.log(this === calculate); // => true
    return this.array.reduce((result, item) => result + item);
  },
};
calculate.sum(); // => 6

/*******************************************************************************/
/**
 *          2. Object Prototype
 *  Similar reason and fix  as object literal
 */
/*******************************************************************************/

// Wrong
function MyCat(name) {
  this.catName = name;
}
MyCat.prototype.sayCatName = () => {
  console.log(this === window); // => true
  return this.catName;
};
const cat = new MyCat('Mew');
cat.sayCatName(); // => undefined

//Right

function MyCat(name) {
  this.catName = name;
}
MyCat.prototype.sayCatName = function () {
  console.log(this === cat); // => true
  return this.catName;
};
const cat_two = new MyCat('Mew');
cat_two.sayCatName(); // => 'Mew'

/*******************************************************************************/
/**
 *         3. Callback Function
 */
/*******************************************************************************/

/**
 *   Avoid using arrow function with event listener callback since context will be dynamic
 *   Below the context of this inside the arrow function is window. When click event happens
 *   browser tries to invoke the handler function with button context, but arrow function
 *   does not change its pre-defined context this.innerHTML is equivalent to window.innerHTML
 *   and has no sense.
 */

// wrong
const button_one = document.getElementById('myButton');
button_one.addEventListener('click', () => {
  console.log(this === window); // => true
  this.innerHTML = 'Clicked button';
});

// right
/**
 *  use function expression instead , which allows to change this depending on the target element
 *  When user clicks the button, this in the handler function is button. Thus innerHTML = 'Clicked button'
 *  modifies correctly the button text to reflect clicked status.
 */

const button_two = document.getElementById('myButton');
button_two.addEventListener('click', function () {
  console.log(this === button_two); // => true
  this.innerHTML = 'Clicked button';
});

/*******************************************************************************/
/**
 *         3. when you need a function to bind to an object
 */
/*******************************************************************************/

// Wrong
// In arrow function 'this' will refer to window object instead of person object
const person = {
  points: 25,
  score: () => {
    this.points++;
  },
};

//correct
//using function expression or es5/6 method declaration resolve this issue
const person_two = {
  points: 25,
  score() {
    this.points++;
  },
};

/**************************************************************************/
/****************** Classical Inheritance class based *********************/
/**************************************************************************/

class Person {
  constructor(name) {
    this.name = name;
  }

  describe() {
    return `Person called ${this.name}`;
  }
}

class EmployeeOne extends Person {
  constructor(name, title) {
    super(name);
    this.title = title;
  }

  describe() {
    return `${super.describe()} and title is ${this.title}`;
  }
}

/**************************************************************************/
/************************ prototypal Inheritance ***************************/
/**************************************************************************/

function Person(name) {
  this.name = name;
}

Person.prototype.describe = function () {
  return `Person called ${this.name}`;
};

function EmployeeTwo(name, title) {
  Person.call(this, name);
  this.title = title;
}

EmployeeTwo.prototype = Object.create(Person.prototype);
EmployeeTwo.prototype.constructor = EmployeeTwo;

EmployeeTwo.prototype.describe = function () {
  return `${super.describe()} and title is ${this.title}`;
};

let emp = new EmployeeTwo('Ram', 'VP');

console.log(emp.describe());

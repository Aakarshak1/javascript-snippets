const promise1 = new Promise<string>((resolve, reject) => {
  console.log('promise1-1');
  resolve('success');
});

promise1.then(() => {
  console.log('promise1-2');
});
console.log('promise1-3');

// promise1 - 1;
// promise1 - 3;
// promise1 - 2;  // it will print this once the promise is resolved

const promise2 = new Promise((resolve, reject) => {
  console.log('promise2-1');
});
promise2.then(() => {
  console.log('promise2-2');
});
console.log('promise2-3');

//promise2-1
// promise2-3
/*
  Why 'promise2-2' is not printed?
  ==> The then method of a promise object only runs if the promise is resolved successfully
      Above the promise is not resolved at all so then method is not get called and since 
      promise will always in pending state so this will not get print.

      how to fix it? see below
*/

const promise3 = new Promise<void>((resolve, reject) => {
  console.log('promise3-1');
  resolve();
});
promise3.then(() => {
  console.log('promise3-2');
});
console.log('promise3-3');

// promise3 - 1;
// promise3 - 3;
// promise3 - 2;

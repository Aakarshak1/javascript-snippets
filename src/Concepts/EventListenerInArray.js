//The EventListener interface represents an object that can handle an event dispatched by an object.

Array.prototype.listener = new Map();

Array.prototype.addListener = function (eventName, cb) {
  if (!this.listener.has(eventName)) {
    this.listener.set(eventName, []);
  }

  let cbList = this.listener.get(eventName);
  cbList.push(cb);
  this.listener.set(eventName, cbList);
};

// new push event, calls trigger event
Array.prototype.pushWithEvent = function () {
  const size = this.length;

  const argsList = Array.prototype.slice.call(arguments);
  for (let index = 0; index < argsList.length; index++) {
    this[size + index] = argsList[index];
  }
  // trigger add event
  this.triggerEvent('add', argsList);
};

Array.prototype.triggerEvent = function (eventName, elements) {
  if (this.listener.has(eventName)) {
    const cbList = this.listener.get(eventName);

    cbList.forEach((cb) => {
      cb(eventName, elements, this);
    });
  }
};

// example
const a = [];
a.addListener('add', (items, args) => {
  console.log('items were added', args);
});
a.pushWithEvent(1, 2, 3, 'a', 'b');
a.pushWithEvent('hello');
a.pushWithEvent(55);
setTimeout(() => {
  a.pushWithEvent('delayed');
}, 2000);

setTimeout(() => {
  console.log(a);
}, 2500);

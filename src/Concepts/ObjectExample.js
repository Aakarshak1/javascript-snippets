// JavaScripts objects have a special property of storing anything as a value.
// It means we can store a list, another object, a function etc as a value.
var marks = {};
var marks = new Object();
var marks = { physics: 98, maths: 95, chemistry: 91 };

// You can easily convert a given object into a JSON string and also reverse it back using JSON object’s stringify and parse methods respectively.
// Return all the keys of object using  Object.keys
const _keys = Object.keys(marks);
var highScore = 0;
for (i of _keys) {
  if (marks[i] > highScore) highScore = marks[i];
}

// Object.values returns the list of values of an object.
const _values = Object.values(marks);

// Object.prototype provides more important functions that have many applications
//  Object.prototype.hasOwnProperty is useful to find out whether a given property/key exists in an object.
marks.hasOwnProperty('physics'); // returns true
marks.hasOwnProperty('greek'); // returns false

// - Object.prototype.instanceof evaluates whether a given object is the type of a particular prototype
function Car(make, model, year) {
  this.make = make;
  this.model = model;
  this.year = year;
}
var newCar = new Car('Honda', 'City', 2007);
console.log(newCar instanceof Car); // returns true

// - Object.freeze allows us to freeze an object so that existing properties cannot be modified.
var marks = { physics: 98, maths: 95, chemistry: 91 };
finalizedMarks = Object.freeze(marks);
finalizedMarks['physics'] = 86; // throws error in strict mode
console.log(marks); // {physics: 98, maths: 95, chemistry: 91}

// - Object.isFrozen  is used to find whether a given object is frozen or not.
Object.isFrozen(finalizedMarks); // returns true

// - Object.seal is slightly different from the freeze. It allows configurable properties but won’t allow new property addition or deletion or properties.
var marks = { physics: 98, maths: 95, chemistry: 91 };
Object.seal(marks);
delete marks.chemistry; // returns false as operation failed
marks.physics = 95; // Works!
marks.greek = 86; // Will not add a new property

// - Object.isSealed is used to check whether a given object is sealed or not
Object.isSealed(marks); // returns true

// - Object.constructor  is used to check what is class of given object

// Apply Example 1
let arr: (string | number)[] = ['a', 'b', 'c'];
let numArray: number[] = [1, 3, 5, 7];
arr.push.apply(arr, numArray);
console.log(arr);

// Apply Example 2
/**
 * Greets the user with a given greeting and language.
 * @param this An object with a name and age property.
 * @param greeting The greeting to display.
 * @param lang The language of the greeting.
 * @returns void
 */
function greet(this: { name: string; age: number }, greeting: string, lang: string): void {
  console.log(lang);
  console.log(`${greeting}, I am ${this.name} and I am ${this.age} years old`);
}

type personType = {
  name: string;
  age: number;
};

const john: personType = {
  name: 'John',
  age: 30,
};

const joe: personType = {
  name: 'Joe',
  age: 40,
};

greet.apply(john, ['Hi', 'en']);
greet.apply(joe, ['Hola', 'es']);

// Call Example 1
//With call() you can write a method once and then inherit it in
// another object without having to rewrite the method for new object
type productType = { name: string; price: number };
function Product(this: productType, name: string, price: number): void {
  this.name = name;
  this.price = price;
}

type foodType = productType & { category: 'food' };
function Food(this: foodType, name: string, price: number) {
  Product.call(this, name, price);
  this.category = 'food';
}

type toyType = productType & { category: 'toy' };
function Toy(this: toyType, name: string, price: number) {
  Product.call(this, name, price);
  this.category = 'toy';
}

let food1 = new (Food as any)('cheese', 5);
let toy1 = new (Toy as any)('robot', 40);

console.log(food1.name);
console.log(food1.category);

console.log(toy1.name);

// call example 2
function greetCall(this: { name: string }): void {
  console.log(`Hello, ${this.name}!`);
}

const person: { name: string } = {
  name: 'John',
};

greetCall.call(person); // logs "Hello, John!"

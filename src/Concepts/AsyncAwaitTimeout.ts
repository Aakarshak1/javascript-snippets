function job(): Promise<String> {
  return new Promise<String>((resolve, reject) => {
    setTimeout(resolve, 1000, 'Hello World 1');
  });
}

async function test(): Promise<String> {
  //code execution will pause here until the promise return by job is resolved
  let message: String = await job();

  console.log(message);
  return 'Hello World 2';
}

test().then((msg: String): void => console.log(msg));

// type numberArr = number[];
// type objectArr = { id: number; ele: number }[];

/************  Cloning an array ************/
let src = [1, 2, 3];
let clonedArr = [...src]; // using spread
let clonedArr_two = src.slice(0); // Slice return new array without altering new array

/************  Cloning an array of object with primitive data ************/
let src_one = [{ a: 1 }, { b: 1 }, { c: 1 }];
let clonedArr_three = src_one.map((item) => ({ ...item }));

/************ Adding element in array ************/
let newArr = [...src, 4];
let newArr_one = [...src_one, { d: 4 }];

/************ Removing element from an array ************/
let element = 3;
let newArr_two = src.filter((item) => item != element);

/************ Replacing element from an array ************/
let element_two = { id: 2, a: 4 };
let src_two = [
  { id: 1, a: 1 },
  { id: 2, a: 2 },
  { id: 3, a: 3 },
];

// replacing without caring about position
let newArr_three = [...src_two.filter((item) => item.id !== element_two.id), element_two];

//replacing caring about position -1
let index = src_two.findIndex(({ id }) => id == element_two.id);
let newArr_four = Object.assign([...src_two], { [index]: element_two });

//replacing caring about position -2
let newArr_five = [...src_two.slice(0, index), element_two, ...src_two.slice(index + 1)];

// sorting element
// native Array.sort() will mutate the original array
const characters = ['Obi-Wan', 'Vader', 'Luke'];
const sortedCharacters = characters.slice().sort();

/**
 * Currying is a technique in functional programming where a function with multiple arguments is
 * transformed into a sequence of functions that each take a single argument. Allowing more flexible and
 * reusable code, and simplifying function composition and chaining.
 *
 * Currying is a transformation of functions that translates a function from callable
 * as f(a, b, c) into callable as f(a)(b)(c).
 */

// Example 1
function multiplyBy(num: number): Function {
  return function (x: number): number {
    return num * x;
  };
}

const multiplyBy2 = multiplyBy(2);
const multiplyBy3 = multiplyBy(3);

console.log(multiplyBy2(5));
console.log(multiplyBy3(5));

// Example 2
function curry_1(f: Function): Function {
  // curry(f) does the currying transform
  return function (a: Number): Function {
    return function (b: Number): Function {
      return f(a, b);
    };
  };
}

function curry_generic<A, B, C>(fn: (a: A, b: B) => C): (a: A) => (b: B) => C {
  return function (a: A): (b: B) => C {
    return function (b: B): C {
      return fn(a, b);
    };
  };
}

// usage
function sum(a: number, b: number) {
  return a + b;
}

let curriedSum = curry_1(sum);
console.log(curriedSum(1)(2));

// Example 3
type user = {
  id: number;
  name: string;
  email: string;
};
const list: user[] = [
  {
    id: 1,
    name: 'Steve',
    email: 'steve@example.com',
  },
  {
    id: 2,
    name: 'John',
    email: 'john@example.com',
  },
  {
    id: 3,
    name: 'Pamela',
    email: 'pam@example.com',
  },
  {
    id: 4,
    name: 'Liz',
    email: 'liz@example.com',
  },
];

//without currying
const noJohn: user[] = list.filter((item) => item.name !== 'John');

// with currying
const filtering =
  (name: string): ((item: user) => boolean) =>
  (item: user) =>
    item.name !== name;

const filterByName = (list: user[], name: string): user[] => list.filter(filtering(name));

console.log(filterByName(list, 'John'));

//Partial Function => a function is partially applied when it is given fewer arguments than
//it expects and returns a new function expecting the remaining arguments

const addPartial = (x: number, y: number, z: number): number => {
  return x + y + z;
};
const partialFunc: Function = addPartial.bind(this, 2, 3);
partialFunc(5); //returns 10

let drinks = ['Cola', 'Lemonade', 'Coffee', 'Water'];

// using splice and indexof ==> it will mutate the original array
const id = drinks.indexOf('Coffee'); // 2
const removedDrink = drinks.splice(id, 1);

// Using filter ==> It will not mutate the original array
drinks = ['Cola', 'Lemonade', 'Coffee', 'Water'];
const filteredDrinks = drinks.filter((drink, index) => index !== id);

// Avoid using the “delete” keyword
// since it will not change the array length
drinks = ['Cola', 'Lemonade', 'Coffee', 'Water'];
delete drinks[id];
console.log(drinks); // the length will always be 4

let arr = [
  { id: 1, name: 'Bobby', test: 'abc' },
  { id: 2, name: 'Harry', test: 'xyz' },
];

// using for each
arr.forEach((object) => {
  delete object['test'];
});

// using array map
const newArr = arr.map(({ id, name }) => {
  return { id, name };
});

// using for...of loop
for (const obj of arr) {
  delete obj['test'];
}

// Emptying the Array

// By setting empty array
drinks = ['Cola', 'Lemonade', 'Coffee', 'Water'];
drinks = [];

// By setting length to 0
drinks = ['Cola', 'Lemonade', 'Coffee', 'Water'];
drinks.length = 0;

// Using splice
drinks = ['Cola', 'Lemonade', 'Coffee', 'Water'];
drinks.splice(0, drinks.length);

// Using while and pop
drinks = ['Cola', 'Lemonade', 'Coffee', 'Water'];
while (drinks.length) {
  drinks.pop();
}

// What will be the output and explain why
/*
    output will be 5 5 5 5 5

    The below code schedules five setTimeout functions with different delay times. 
    The delay times are based on the current value of i multiplied by 1000 milliseconds.

    But because the way JavaScript closures and variable scoping works, when the callback function will get executed 
    they will all refer to the same value of variable i which is 5 at that point ( since the for loop will have already finished by then)

    Therefore, when the console.log(i) is executed inside each callback, it will output 5 for all iterations. 
    This is because the value of i is shared among all the callback functions, and by the time they are executed, 
    the loop has already finished and i has reached its final value of 5.
*/

for (var i: number = 0; i < 5; i++) {
  setTimeout(function () {
    console.log(i);
  }, i * 1000);
}

// To output 1 2 3 4 5
/*
    Approach 1. Using closure to capture the value of i in each iteration

    we will create IIFE that will take `i` as parameter and immediately invokes it with the current value of `i` plus 1
    IIFE will create new scope for each iteration of the loop, capturing the value of `i` at that specific iteration
    inside IIFE, pass the captured value as an argument to the callback of settimeout. this way each callback has its own unique value 
*/

for (var i: number = 0; i < 5; i++) {
  (function (j: number) {
    setTimeout(() => {
      console.log(j);
    }, j * 1000);
  })(i + 1);
}

/**
 *  Approach 2. Using let
 *  let will create a new block scope for each iteration of the loop, ensuring that the value of `i` scoped to that block
 *  with this each iteration wof the loop will have its own unique `i` value, and the setTimeout callback function will
 *  correctly capture the value of i at that particular iteration.
 *
 *  output is 1 2 3 4 5
 */

for (let i: number = 0; i <= 5; i++) {
  setTimeout(() => {
    console.log(i);
  }, i * 1000);
}

/***************************************************************************/
/********************* Question 2 *****************************************/
/***************************************************************************/

const arr = [10, 12, 15, 21];
for (var i = 0; i < arr.length; i++) {
  setTimeout(function () {
    console.log('Index: ' + i + ', element: ' + arr[i]);
  }, 1000);
}

/**
 output 
Index: 4, element: undefined
Index: 4, element: undefined
Index: 4, element: undefined
Index: 4, element: undefined
 * 
 * The reason for this is because the setTimeout function creates a function (the closure) that has 
 * access to its outer scope, which is the loop that contains the index i. After 3 seconds go by, 
 * the function is executed and it prints out the value of i, which at the end of the loop is at 4 
 * because it cycles through 0, 1, 2, 3, 4 and the loop finally stops at 4.arr[4] does not exist, 
 * which is why you get undefined.
 */

//  Solution 1  pass the needed parameters into the inner function

for (var i = 0; i < arr.length; i++) {
  // pass in the variable i so that each function
  // has access to the correct index
  setTimeout(
    (function (i_local) {
      return function () {
        console.log('Index: ' + i_local + ', element: ' + arr[i_local]);
      };
    })(i),
    1000
  );
}

//solution 2 using Let Keyword
for (let i = 0; i < arr.length; i++) {
  // using the ES6 let syntax, it creates a new binding
  // every single time the function is called
  setTimeout(function () {
    console.log('Index: ' + i + ', element: ' + arr[i]);
  }, 1000);
}

for (let i = 0; i < 10; i++) {
  setTimeout(() => console.log(i), 1000);
}

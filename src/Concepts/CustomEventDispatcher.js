class EventDispatcher {
  constructor() {
    this.listener = new Map();
  }

  addEventListener(evtType, cb) {
    if (!this.listener.has(evtType)) {
      this.listener.set(evtType, []);
    }

    let cbList = this.listener.get(evtType);
    if (cbList.indexOf(cb) === -1) cbList.push(cb);
    this.listener.set(evtType, cbList);
  }

  removeEventListener(evtType, listener) {
    if (!this.listener.size || !this.listener.has(evtType)) return;

    let cbList = this.listener.get(evtType);
    const index = cbList.indexOf(listener);

    if (index !== -1) {
      cbList.splice(index, 1);
      this.listener.set(evtType, cbList);
    }
  }

  dispatchEvent(event) {
    if (!this.listener.size || !this.listener.has(event.type)) return;

    let cbList = this.listener.get(event.type);

    const eventListenersCopy = cbList.slice(0);
    event.target = this;

    for (let i = 0; i < eventListenersCopy.length; i++) {
      eventListenersCopy[i].call(this, event);
    }
  }
}

const ed = new EventDispatcher();
// DO NOT USE ON PRODUCTION. ONLY FOR EXAMPLE
const event = {
  target: null,
  type: 'my-event',
};
const listener = () => {
  console.log('handled!');
};
ed.addEventListener('my-event', listener);
// should be handled
ed.dispatchEvent(event);
ed.removeEventListener('my-event', listener);
// should not be handled
ed.dispatchEvent(event);

class MyEventEmitter {
  constructor() {
    this._events = new Map();
  }

  on(eventName, callbackListener) {
    if (!this._events.has(eventName)) {
      this._events.set(eventName, []);
    }

    let callBackList = this._events.get(eventName);
    callBackList.push(callbackListener);
    this._events.set(eventName, callBackList);
  }

  once(eventName, cb) {
    if (!this._events.has(eventName)) {
      this._events.set(eventName, []);
    }

    const onceWrapper = () => {
      cb();
      this.removeListener(eventName, cb);
    };
    this._events.set(eventName, onceWrapper);
  }

  removeListener(eventName, cbToRemove) {
    if (!this._events.has(eventName))
      throw new Error(`Can't remove listener. Event ${eventName} does not exists`);

    let callBackList = this._events.get(eventName);
    callBackList = callBackList.filter((cb) => cb !== cbToRemove);
    this._events.set(eventName, callBackList);
  }

  emit(eventName, data) {
    if (!this._events.has(eventName))
      throw new Error(`Can't emit event. Event ${eventName} does not exists`);

    let callBackList = this._events.get(eventName);

    callBackList.forEach((cb) => {
      cb(data);
    });
  }

  listenerCount(eventName) {
    let callBackList = this._events.get(eventName);
    let count = callBackList?.length ?? 0;
    return count;
  }
}

const myEventEmitter = new MyEventEmitter();
const handleMyEvent = (data) => {
  console.log('Was fired: ', data);
};

myEventEmitter.on('testEvent', handleMyEvent);
myEventEmitter.emit('testEvent', 'hi'); // Was fired: hi
myEventEmitter.emit('testEvent', 'trigger'); // Was fired: trigger
myEventEmitter.emit('testEvent', 'again'); // Was fired: again

myEventEmitter.removeListener('testEvent', handleMyEvent);

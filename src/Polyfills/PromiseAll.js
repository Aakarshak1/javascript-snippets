const promise_one = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('resolve promise one');
  }, 1000);
});

const promise_two = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('resolve promise two');
  }, 1000);
});

const promise_three = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('resolve promise three');
  }, 1000);
});

function createTaskFn(time) {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      resolve(time);
    }, time);
  });
}

/**
 1. Since the order of the results array is important, we will chain each promise resolution to the next one
 
 2. A simple forEach iterator can be used, and we need to keep track of the index of the parent array
 
 3. When the number of promises resolved is equal to the length of the task array we will resolve the overall results array.
 */
Promise.myPromiseAll = function (values) {
  return new Promise((resolve, reject) => {
    let result = []; //Array to store our resolved value
    let total = 0; // counter ro keep track of how many promise have been resolved

    values.forEach((promiseItem, index) => {
      Promise.resolve(promiseItem)
        .then((res) => {
          result[index] = res;
          total++;

          //if all promises are resolved then resolved the main promise
          if (total === values.length) resolve(result);
        })
        .catch((err) => reject(err)); // if any of promise throws an error reject the main promise with error
    });
  });
};

const taskList = [createTaskFn(1000), createTaskFn(5000), createTaskFn(3000)];

Promise.myPromiseAll(taskList)
  .then((res) => console.log(res))
  .catch((err) => console.log(err));

// Promise.myPromiseAll([promise_one, promise_two, promise_three])
//   .then((res) => console.log(res))
//   .catch((err) => console.log(err));

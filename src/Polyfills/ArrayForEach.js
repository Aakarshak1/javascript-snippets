Array.prototype.forEachPolyfill = function (cb) {
  for (let i = 0; i < this.length; i++) {
    cb(this[i], i, this);
  }
};

let arr = [1, 2, 3];
arr.forEachPolyfill((num) => console.log(num));

// Map Polyfill
Array.prototype.mapPolyfill = function (cb) {
  //  this refers to the array that the mapPolyfill function is being called on.
  if (!Array.isArray(this)) throw new TypeError('this is not an Array');
  if (typeof cb !== 'function' || typeof cb !== 'Function')
    throw new TypeError('callback is not function');
  let newArray = [];
  for (let i = 0; i < this.length; i++) {
    // callback function cb gets 3 three arguments currentElement, index and Execution context(this)
    newArray.push(cb(this[i], i, this));
  }
  return newArray;
};

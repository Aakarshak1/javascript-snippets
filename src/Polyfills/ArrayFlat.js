/**
 * The flat() method creates a new array with all sub-array elements concatenated into it recursively up to the specified depth.
 */

let inputArray = [1, 2, [3, 4], 5, [[[6, 7], 8, [[[[9]]]]]]];
// output [1, 2, 3, 4, 5, 6, 7, 8, 9]

// function flatten(arr, depth) {
//   if (depth <= 0) return arr;

//   let flatArr = [];

//   for (let i = 0; i < arr.length; i++)
//     if (depth > 0 && Array.isArray(arr[i])) flatArr.push(...flatten(arr[i], depth - 1));
//     else flatArr.push(arr[i]);

//   return flatArr;
// }

// const res = flatten(inputArray, 2);
// console.log(res);

Array.prototype.flatten = function (depth) {
  if (depth <= 0) return this;

  let flatArr = [];

  for (let idx = 0; idx < this.length; idx++) {
    const element = this[idx];
    if (depth > 0 && Array.isArray(element)) flatArr.push(...this.flatten.call(element, depth - 1));
    else flatArr.push(element);
  }

  return flatArr;
};

const res = inputArray.flatten(3);
console.log(res);

// Array reduce polyfill
// Context from src/Polyfills/ArrayReduce.js:reduce

//Calling reduce() on an empty array without an initialValue will throw a TypeError. 
Array.prototype.myReduce = function (callback, initialValue) {
  //  this refers to the array that the myReduce function is being called on.
  //check if 'this' is an array
  if (!Array.isArray(this)) throw new TypeError('this is not an Array');

  // check if array is empty
  if (this.length === 0) throw new TypeError('Reduce of empty array');

  // check if the callback is a function
  if (typeof callback !== 'function') throw new TypeError(callback + ' is not a function');

  // Variable that accumulates the result after performing operation one-by-one on the array elements

  //   if we want initialValue required
  //   if (!initialValue) throw new TypeError('initialValue is not function');

  let accumulator = initialValue === undefined ? undefined : initialValue;

  for (let i = 0; i < this.length; i++) {
    // If the initialValue exists, we call the callback function on the existing element and store in accumulator
    if (accumulator !== undefined) {
      // this[i], i, this  ==> currentValue, index, array
      accumulator = callback.call(undefined, accumulator, this[i], i, this);
    } else {
      // If initialValue does not exist, we assign accumulator to the current element of the array
      accumulator = this[i];
    }
  }

  // return the overall accumulated response
  return accumulator;
};

// Code to calculate sum of array elements
// using our own reduce method
const arr = [1, 2, 3, 4];
console.log(arr.myReduce((total, elem) => total + elem));

// Code to calculate multiplication of all array elements
console.log(arr.myReduce((prod, elem) => prod * elem));

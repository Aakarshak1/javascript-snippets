Array.prototype.FilterPolyfill = function (cb) {
  //  this refers to the array that the FilterPolyfill function is being called on.
  if (!Array.isArray(this)) throw new TypeError('this is not an Array');
  if (!((typeof cb === 'Function' || typeof cb === 'function') && this))
    throw new TypeError('callback is not function');

  let newArray = [];

  for (let i = 0; i < this.length; i++) {
    let result = cb(this[i], i, this); // currentValue, index, array
    // let result = cb(this[i]); //currentValue

    if (result) newArray.push(this[i]);
  }

  return newArray;
};

let arr = [1, 2, 4, 5, 9, 6];

let evenArr = arr.FilterPolyfill((num) => num % 2 === 0);
// let error = arr.FilterPolyfill();
// console.log(error);
console.log(evenArr);

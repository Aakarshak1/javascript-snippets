// args take 2 arguments, one being the boundContext and another an optional parameter of an array of argument
Function.prototype.myBind = function (...args) {
  // Store the original function that the myBind method was called on
  let thisContext = this;
  // Store the first argument passed to the myBind method here `obj`
  let boundContext = args[0];
  // Store the remaining arguments passed to the myBind method
  let params = args.slice(1);

  if (typeof thisContext !== 'function') {
    throw new TypeError(`${thisContext} is not a function`);
  }
  // Return a new function with the specified this value and arguments.
  return function (...args2) {
    thisContext.apply(boundContext, params.concat(args2));
  };
};

function showProfileMessage(hometown, country) {
  console.log(`${this.name}`);
  console.log(`${this.name} ${hometown}`);
  console.log(`${this.name} ${hometown} ${country}`);
  console.log('\n');
}
const obj = {
  name: 'John Doe',
};
let print = showProfileMessage.myBind(obj);
print();
print('NYC');
print('Delhi', 'India');

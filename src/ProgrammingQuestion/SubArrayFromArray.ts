/**
 *  Find all the sub-array from given array
 */

let arr = [1, 2, 3, 4];

/**
[ 1 ]
[ 1, 2 ]
[ 1, 2, 3 ]
[ 1, 2, 3, 4 ]
[ 2 ]
[ 2, 3 ]
[ 2, 3, 4 ]
[ 3 ]
[ 3, 4 ]
[ 4 ]
 */

function getAllSubArrays(arr: number[]) {
  const result = [];
  const len = arr.length;
  let temp = [];

  // Using array slice
  // for (let i = 0; i < len; i++) {
  //   for (let j = i + 1; j <= len; j++) {
  //     result.push(arr.slice(i, j));
  //   }
  // }

  // for (let i = 0; i < len; i++) {
  //   for (let j = i; j < len; j++) {
  //     temp = [];
  //     for (let k = i; k <= j; k++) {
  //       temp.push(arr[k]);
  //     }
  //     result.push(temp);
  //   }
  // }

  for (let i = 0; i < len; i++) {
    for (let j = 0; j < len - i; j++) {
      temp = [];
      for (let k = i; k <= i + j; k++) {
        temp.push(arr[k]);
      }
      result.push(temp);
    }
  }

  return result;
}

const subArrays = getAllSubArrays(arr);
console.log(subArrays);

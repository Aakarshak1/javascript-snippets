/* 
   Given an array that contains only numbers in the range from 1 to arr.length, find the first duplicate number 
   for which the second occurrence has the minimal index. In other words, if there are more than 1 duplicated 
   numbers, return the number for which the second occurrence has a smaller index than the second occurrence of
   the other number does. If there are no such elements, return -1.

   arr [2, 1, 3, 5, 3, 2] output ==> 3
*/

function firstDuplicate(arr: number[]): number {
  const uniqueValue = new Set();

  for (let num of arr)
    if (uniqueValue.has(num)) return num;
    else uniqueValue.add(num);

  return -1;
}

const arr_one: number[] = [2, 1, 3, 5, 3, 2];

console.log(firstDuplicate(arr_one));

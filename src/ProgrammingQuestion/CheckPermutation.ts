// export {}; //makes it a module and so it has its own module scope and can thus use name without a class.
/*
 *  Given Two strings, write a method to decide if one is permutation of the other. // same characters, but different order
 */

let string1: string = 'test';
let string2: string = 'text';
let string3: string = 'test';

function checkPermutation(string1: string, string2: string): boolean {
  if (string1.length !== string2.length) return false;

  string1 = string1.split('').sort().join('');
  string2 = string2.split('').sort().join('');

  if (string1 === string2) return true;
  else return false;
}

const result1: boolean = checkPermutation(string1, string2); // false

const result2: boolean = checkPermutation(string1, string3); // true

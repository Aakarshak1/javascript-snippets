function job(): Promise<string> {
  return new Promise((resolve, reject) => {
    setTimeout(resolve, 1000, 'Hello World ');
  });
}

async function main(): Promise<string> {
  const result = await job();
  console.log(result);

  return 'Hello 2';
}

main().then((msg) => console.log(msg));

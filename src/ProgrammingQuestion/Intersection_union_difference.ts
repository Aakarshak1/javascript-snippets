let arrA: number[] = [1, 3, 4, 5];
let arrB: number[] = [2, 3, 6, 7];

// find intersection of two arrays
function intersection_1(arrA: number[], arrB: number[]): number[] {
  let intersection: number[] = [];
  let setA: Set<number> = new Set(arrA);
  let setB: Set<number> = new Set(arrB);
  for (let num of setA) {
    if (setB.has(num)) {
      intersection.push(num);
    }
  }
  return intersection;
}

function intersection_2(arrA: number[], arrB: number[]): number[] {
  return arrA.filter((num) => arrB.includes(num));
}

console.log(intersection_1(arrA, arrB));
console.log(intersection_2(arrA, arrB));

// find union of two arrays
function union_1(arrA: number[], arrB: number[]): number[] {
  let union: number[] = [];
  let setA: Set<number> = new Set(arrA);
  let setB: Set<number> = new Set(arrB);
  for (let num of setA) {
    union.push(num);
  }
  for (let num of setB) {
    union.push(num);
  }
  return union;
}

function union_2(arrA: number[], arrB: number[]): number[] {
  return [...new Set([...arrA, ...arrB])];
}

console.log(union_1(arrA, arrB));
console.log(union_2(arrA, arrB));

//elements from array A that are not in the array B
function difference_1(arrA: number[], arrB: number[]): number[] {
  let difference: number[] = [];
  let setA: Set<number> = new Set(arrA);
  let setB: Set<number> = new Set(arrB);
  for (let num of setA) {
    if (!setB.has(num)) {
      difference.push(num);
    }
  }
  return difference;
}

function difference_2(arrA: number[], arrB: number[]): number[] {
  return arrA.filter((num) => !arrB.includes(num));
}

console.log(difference_1(arrA, arrB));
console.log(difference_2(arrA, arrB));

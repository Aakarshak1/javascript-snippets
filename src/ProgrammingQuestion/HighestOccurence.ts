/**
 * Given an array, find the most frequent element in it.
 * If there are multiple elements that appear a maximum number of times, print any one of them.
 */

// Approach 1 Using Map -- O(n) where n is length of array
const highestOccurrenceSolutionOne = (arr: number[]): number | undefined => {
  let maxCount = 0;
  let occurrence = new Map();

  arr.forEach((num) => {
    const count: number = occurrence.has(num) ? occurrence.get(num) + 1 : 1;
    occurrence.set(num, count);
    maxCount = count > maxCount ? count : maxCount;
  });

  return arr.find((num) => occurrence.get(num) === maxCount);
};

// console.log(highestOccurrenceSolutionOne([25, 50, 50, 25, 75, 100]));
// console.log(highestOccurrenceSolutionOne([0, 9, 1, 2, 1, 4, 2, 0, 5]));
// console.log(highestOccurrenceSolutionOne([40, 50, 30, 40, 50, 30, 30]));

//Approach 2 using for loop
// time complexity is  O(n2) since two for loop is running
const highestOccurrenceSolutionTwo = (arr: number[]): number | undefined => {
  let maxCount: number = 0;
  let elementMaxFrequent: undefined | number = undefined;

  for (let i: number = 0; i < arr.length; i++) {
    let count: number = 0;
    for (let j: number = 0; j < arr.length; j++) {
      if (arr[i] == arr[j]) count++;
    }

    if (count > maxCount) {
      maxCount = count;
      elementMaxFrequent = arr[i];
    }
  }
  return elementMaxFrequent;
};

// console.log(highestOccurrenceSolutionTwo([25, 50, 50, 25, 75, 100]));
// console.log(highestOccurrenceSolutionTwo([0, 9, 1, 2, 1, 4, 2, 0, 5]));
// console.log(highestOccurrenceSolutionTwo([40, 50, 30, 40, 50, 30, 30]));

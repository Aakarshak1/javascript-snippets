/**
 * Write a script to continuously print the message “Hello World” with the same varying delays 
 * concept as challenge #3, but this time, in groups of 5 messages per main-delay interval. 
 * Starting with a delay of 100ms for the first 5 messages, then a delay of 200ms for the next 
 * 5 messages, then 300ms, and so on.
    Here’s how the script should behave:
    1. At the 100ms point, the script will start printing “Hello World” and do that 5 times with 
      an interval of 100ms. The 1st message will appear at 100ms, 2nd message at 200ms, and so on.
    2. After the first 5 messages, the script should increment the main delay to 200ms. So 6th 
      message will be printed at 500ms + 200ms (700ms), 7th message will be printed at 900ms, 
      8th message will be printed at 1100ms, and so on.
    3. After 10 messages, the script should increment the main delay to 300ms. So the 11th message 
      should be printed at 500ms + 1000ms + 300ms (18000ms). The 12th message should be printed at 
      21000ms, and so on.

      Constraints: You can use only setInterval calls (not setTimeout) and you can use only ONE if statement.

 */

let lastInterval: ReturnType<typeof setInterval>;
let counter: number = 5;

const greeting = (delay: number) => {
  if (counter === 5) {
    clearInterval(lastInterval);
    lastInterval = setInterval(() => {
      console.log(`Hello World. ${delay}`);
      greeting(delay + 100);
    }, delay);
    counter = 0;
  }
  counter += 1;
};

greeting(100);

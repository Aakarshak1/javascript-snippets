/**
 *  Sort the array with the frequency of item in given array
 */

function itemSort(items: number[]) {
  //  keep track of the frequency of each item in the array
  const frequency: Record<number, number> = {};
  items.forEach((element: number) => {
    frequency[element] = (frequency[element] || 0) + 1;
  });

  // map each item in items array with an array containing item itself and its frequency
  const sorted: [number, number][] = items.map((item) => [item, frequency[item]]);

  /**
   *  array.sort((a, b) => a - b);
   *   a-b < 0 ==> a should be sorted / placed before b
   *   a-b > 0 ==> b should be sorted / placed before a
   *   a-b = 0  ==> no change in position of a and b
   */
  // sort element based on the frequency of the item
  sorted.sort((a, b) => {
    if (a[1] === b[1]) return a[0] - b[0];
    return a[1] - b[1];
    // return a[0] - b[0]; // it will only sort the number not according to their frequency which is wrong
  });

  // Map item from sorted array
  const result: number[] = sorted.map((s) => s[0]);
  console.log(result);
}

const itemSort1 = [4, 5, 6, 5, 4, 3];
const itemSort2 = [3, 1, 2, 2, 4];
const itemSort3 = [8, 5, 5, 5, 5, 1, 1, 1, 4, 4];
itemSort(itemSort1); // [3, 6, 4, 4, 5, 5];
itemSort(itemSort2); // [ 1, 3, 4, 2, 2 ]
itemSort(itemSort3); // [ 8, 4, 4, 1, 1, 1, 5, 5, 5, 5 ]

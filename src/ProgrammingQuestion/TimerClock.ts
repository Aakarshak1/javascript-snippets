let idVar: NodeJS.Timer = setInterval(() => {
  timer();
}, 1000);

function timer(): void {
  let dateVar: Date = new Date();
  let time: string = dateVar.toLocaleTimeString();
  console.log(time);
}

setTimeout(() => clearInterval(idVar), 10000);

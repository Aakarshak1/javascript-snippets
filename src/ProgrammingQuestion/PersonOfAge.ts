//print blow user list as <username> of <age>

type Person = {
  name: string;
  age: number;
  next: Person | null;
};

const list: Person = {
  name: 'John',
  age: 15,
  next: {
    name: 'Lilly',
    age: 27,
    next: {
      name: 'Simon',
      age: 5,
      next: { name: 'Helen', age: 20, next: null },
    },
  },
};

function printName(person: Person | null): void | Function {
  if (person === null) return;
  const { name, age, next } = person;
  if (next !== null) console.log(`${name} of age ${age}`);
  if (typeof next === 'object') printName(next);
}

printName(list);

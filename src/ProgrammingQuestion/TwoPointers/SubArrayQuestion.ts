/**
    Given Sorted Arr 'arr' of size L and integer B 
    Find the number of subarray of array arr such that if [L, R] defines the range of subarray, then 
    arr[L]+min(arr[L], arr[L+1], .... arr[R]) + arr[R] is greater than or equal to B. 

    input : arr = [2,3,5,8,10]  B= 12
    output :  10
*/
/**
 *   Brute Force approach
 *   Generate all subarray and count those for which arr[L]+min(arr[L], arr[L+1], .... arr[R]) + arr[R]
 *  is greater than or equal to B
 */

let arr: number[] = [2, 3, 5, 8, 10];
let B = 12;

function countSubArrays(arr: number[], B: number) {
  const n = arr.length;
  let count = 0;

  for (let i = 0; i < n; i++) {
    for (let j = i; j < n; j++) {
      // Get the minimum element
      const minElement = Math.min(...arr.slice(i, j + 1));
      // Calculate sum
      const sum = arr[i] + minElement + arr[j];
      if (sum >= B) count++;
    }
  }

  return count;
}

const result = countSubArrays(arr, B);
console.log(result);

/**
 *  Two Pointer one start and one end of array length
 *    Since array is sorted min(arr[L], arr[L+1], .... arr[R]) will be arr[L]  therefore
 *      2 * arr[L] + arr[R] >= B  ==> B - 2 arr[L] <= arr[R]
 */

function usingTwoPointer(arr: number[], B: number) {
  let leftPointer = 0;
  let rightPointer = arr.length - 1;
  let answer = 0;

  while (leftPointer <= rightPointer) {
    let temp = B - 2 * arr[leftPointer];
    if (temp <= arr[rightPointer]) {
      answer += rightPointer - leftPointer + 1;
      rightPointer--;
    } else leftPointer--;
  }

  return answer;
}

const result_two = usingTwoPointer(arr, B);
console.log(result_two);

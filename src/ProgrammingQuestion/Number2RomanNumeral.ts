function convertToRoman(number: number): string {
  // mapping of roman literal
  const roman: { [key: string]: string } = {
    M: '1000',
    CM: '900',
    D: '500',
    CD: '400',
    C: '100',
    XC: '90',
    L: '50',
    XL: '40',
    X: '10',
    IX: '9',
    V: '5',
    IV: '4',
    I: '1',
  };

  // Create an array of the Roman numeral literals
  const literals: string[] = Object.keys(roman);

  // Initialize the index to the last element in the literals array
  let currentIndex: number = literals.length - 1;

  // Create an empty array to store the converted Roman numerals
  let conversion: string[] = [];

  // Convert the number to Roman numerals
  while (number > 0) {
    // Get the current Roman numeral literal
    let currentLiteral: string = literals[currentIndex];
    // Get the corresponding value of the current Roman numeral
    let currentNum: number = +roman[currentLiteral];
    // Calculate the quotient and remainder of dividing the number by the current value
    let quotient: number = Math.floor(number / currentNum);
    number = number % currentNum;

    // Add the current Roman numeral to the conversion array based on the quotient
    for (let i = 0; i < quotient; i++) {
      conversion.push(currentLiteral);
    }

    // Move to the next Roman numeral in the literals array
    currentIndex--;
  }

  // Join the converted Roman numerals into a string representation
  return conversion.join('');
}

let result = convertToRoman(45);

// console.log(result);

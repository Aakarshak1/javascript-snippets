const doWork = async () => {};
console.log(doWork()); //fulfilled but output is Promise { undefined }

/***********************************************************************/
function myFunction(name) {
  this.name = name;
  this.instanceCount = function () {
    this.count++;
    console.log(`${this.name} ${this.count}`);
  };
}

myFunction.prototype.count = 0;

let myFun1 = new myFunction('zz');
myFun1.instanceCount();
myFun1.instanceCount();
myFun1.instanceCount();
myFun1.instanceCount();
myFun1.instanceCount();
myFun1.instanceCount();

/***********************************************************************/

//creating  private variable
//using closure to limit the scope of variable

function privateVariable() {
  let pvt = 'private variable';
  return () => pvt;
}

let pt = privateVariable();

console.log(pt());
/***********************************************************************/

console.log(typeof typeof 1); //string
console.log(typeof 1); //"number"
console.log(typeof 'number'); //string

/***********************************************************************/

console.log(2 + '2'); //number 2 is coverted in string 2 and then concated to string 2 ==> 22
console.log(2 - '2'); //string 2 is converted into number 2 ==> 0

/***********************************************************************/

// remove duplicate from array
let numArr = [3, 22, 14, 40, 22, 15, 18, 3];
let resultArr = numArr.filter((ele, index, array) => array.indexOf(ele) === index);
let resultArr_one = Array.from(new Set(numArr));

/***********************************************************************/

console.log(5 < 6 < 7); //(true < 7 )==> (1<7) ==> true
console.log(5 > 6 > 7); // (false > 7) ==> (0>7) ==> false

/***********************************************************************/

console.log(Math.max()); //-Infinity

/***********************************************************************/
//recursion example
let sum = (a) => (b) => {
  return b ? sum(a + b) : a;
};

let result = sum(1)(2)(3)(4)();

console.log(result);

/***********************************************************************/

const someFn = async () => {
  try {
    const result = await fetch('https://jsonplaceholder.typicode.com/todos/1');
    return result;
  } catch (error) {
    console.log(error);
  }
};

//below line will print Promise in <pending> status (inside result is there in [[PromiseResult]] )
someFn()
  .then((response) => response.json())
  .then(console.log());

//below line will print Promise in <pending> status and result (data) in next line
someFn()
  .then((response) => response.json())
  .then((data) => console.log(data));

/***********************************************************************/
/**
 * Write a script to print the message “Hello World” every second, but only 5 times.
 * After 5 times, the script should print the message “Done” and let the Node process exit.
 *
 * You cannot use setTimeout()
 */

let counter = 0;
const interval = setInterval(() => {
  console.log('Hello World');
  counter++;

  if (counter === 5) {
    console.log('Done');
    clearInterval(interval);
  }
}, 1000);

/***********************************************************************/
/**
 * Write a script to continuously print the message “Hello World” with varying delays. 
 * Start with a delay of 1 second and then increment the delay by 1 second each time. 
 * The second time will have a delay of 2 seconds. The third time will have a delay of 3 seconds, and so on.
    Include the delay in the printed message. Expected output looks like:
Hello World. 1
Hello World. 2
Hello World. 3

Constraints: You can only use const to define variables. You can’t use let or var. you can not use any loop
 * 
 */

const greeting = (delay) =>
  setTimeout(() => {
    console.log(`Hello World ${delay}`);
    greeting(delay + 1);
  }, delay * 1000);

greeting(1);

// below will print into interval of 1 second

const greeting_two = (delay) =>
  setTimeout(() => {
    console.log(`Hello World ${delay}`);
    greeting(delay + 1);
  }, 1000);

greeting(1);

/***********************************************************************/

// if every element of subarray is present in array
let isSubArray = subArr.every((ele) => arr.includes(element));

/***********************************************************************/

// guess the output
/**
   This question is about Scope. In JavaScript, there are two types of scopes: Global Scope and 
   Local Scope, variables declared within a JavaScript function become local, and variables declared 
   outside of the function become global.
   Here var a = 1 declared out of the function and saved in global memory. 
   The var a = 2 declared inside the function and saved in local memory. 
   it's a different place in memory (even if they have the same name).
 */
var a = 1;
function foo() {
  var a = 2;
  console.log(a);
}
foo();
console.log(a);

/***********************************************************************/

/**
    If you said a is not defined, you have healthy thinking, but the answer is 2.
    According to the logic we talked about in the previous question, the variables should be in local 
    scope, But, if you pay attention, the variables didn't declare (without var, let, const). 
    When we do assignments without declarations in JavaScript (a=2),  the compiler will save the variable in the global scope. 
    FYI, we can fix this behavior by add "use strict".
*/
function foo() {
  a = 2;
}
foo();
console.log(a);

/***********************************************************************/

/**
 * The answer is 10. This question is about Closures. In simple words - Closures are functions that
 * return another function, and the inner function has access to the outer variables function
 * (You can read more here). We can look at Closures like Global scope (outer function) and
 * Local scope (inner function) that leaves inside the local scope (baseValue). Like regular Scope
 * in JavaScript, the Local scope has access to Global Scope. For that reason, the compiler can
 * know what is value.
 */

var answer = 0;
const baseValue = (value) => (multipleValue) => value * multipleValue;
const multiple = baseValue(2);
answer = multiple(5);
console.log(answer);

/***********************************************************************/
/**
 The answer is {a: 1,b: 2}. 
 After the previous question, we can recognize it's a Closure, but this question is also about References. 
 
 In JavaScript, there are two kinds of variables types: 
 primitives variables (strings, numbers, and booleans) and 
 References variables (arrays and objects). 

 Primitives variables save in memory in their values, and references save in memory as virtual id. 
 For that reason, when we pass the object from function to function, we actually pass the virtual id. 
 
 In our case, the compiler saved the obj and saw we call the reference again by outerParam["b"] = innerParam. 
 Hence, the added to the object key b and value 2 (value of innerParam).
 */

function outerFunc(outerParam) {
  function innerFunc(innerParam) {
    outerParam['b'] = innerParam;
  }
  return innerFunc;
}

const obj = { a: 1 };
const example = outerFunc(obj);
const answer = example(2);
console.log(obj);

let arr = [1, 2];
function test(array) {
  array.push(3);
}
test(arr);
console.log(arr);

/***********************************************************************/
/**
 * The answer is [1,2]. using the ES6 "Spread Operator," it's basically the same do test([1,2,3]).
 * In this way, we created a new array (new reference) and arr reference saved in different id - we
 * didn't change the reference just added another one.
 */
let arr_one = [1, 2];
function test(array) {
  array.push(3);
  return array;
}
console.log(test([...arr_one]));
console.log(arr_one);

/***********************************************************************/
// in node it will return undefined
/**
 * The answer is Joe.
 * The question's subject is "Context"(this). In JavaScript, this is the object that is called to the function.
 * When we do var getCarName = carDetails.getName; we store the function in the global scope, So 'this' will be Window,
 * and because we set name in the global scope (window), the output will be Joe (same as window.name).
 * This is JavaScript's default behavior. If you want to change this behavior, you can use: bind, apply, call, and arrow functions.
 */
const carDetails = {
  name: 'Tom',
  getName() {
    return this.name;
  },
};
var name = 'Joe';
var getCarName = carDetails.getName;
console.log(getCarName());

/***********************************************************************/

/*
  If you said undefined and b is not defined (error), congratulation, you know Hoisting. In simple 
  words, hoisting is the way the compiler defines the variables. When the function executes, the 
  compiler looks for variables declaration, and if the variable were declared, it would hoist it 
  to the top

  It only works on var. let and const is not "hoisting" variables. 
  This is why we will see the error: b is not defined.
*/
console.log(a);
console.log(b);
var a = 2;
let b = 2;

/***********************************************************************/

/**
 * The answer is a and bb is not a function (error).
 * This is a very wried part of JavaScript - functions are hoisted as well. If you read the last answer, you will
 * understand that var is also hoisted, but it doesn't matter what will be after the = operator.
 * In the hoisting stage, it will always be undefined. So when we do b(), it's the same as undefined().
 */

a();
function a() {
  console.log('a');
}
bb();
var bb = function () {
  console.log('b');
};

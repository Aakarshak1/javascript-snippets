/*
 * Write a program that prints the numbers from 1 to 100. But for multiples of three print “Fizz” instead
 * of the number and for the multiples of five print “Buzz”. For numbers which are multiples of both
 * three and five print “FizzBuzz”.
 */

/*  Solution 1  using for loop and if-else */

for (let i = 0; i <= 100; i++) {
  //multiple of 3 and 5 both
  if (i % 3 === 0 && i % 5 === 0) console.log('FizzBuzz');
  // multiple of 3
  else if (i % 3 === 0) console.log('Fizz');
  // multiple of 5
  else if (i % 5 === 0) console.log('Buzz');
  // print number
  else console.log(i);
}

/* Solution 2  using for loop and if-else and string to concatenate all  output */
// one console.log instead of multiples
function fizzBuzz_two() {
  let output = '';

  for (let i = 0; i <= 100; i++) {
    if (i % 3 === 0 && i % 5 === 0) output += 'FizzBuzz\n';
    else if (i % 3 === 0) output += 'Fizz\n';
    else if (i % 5 === 0) output += 'Buzz\n';
    else output += `${i}\n`;
  }

  return output;
}

/* Solution 3 Using Switch with helper function   */

const isMultiple = (num: number, mod: number) => num % mod === 0;

function fizzBuzz_three() {
  let output = '';
  for (let i = 1; i <= 20; i++) {
    switch (true) {
      case isMultiple(i, 15):
        output += 'FizzBuzz ';
        break;
      case isMultiple(i, 3):
        output += 'Fizz ';
        break;
      case isMultiple(i, 5):
        output += 'Buzz ';
        break;
      default:
        output += `${i} `;
        break;
    }
  }
  return output;
}

/* Solution 4 Spread + Map + Switch   */

const main = [...Array(100)]
  .map((_, i) => {
    const num = i + 1;
    switch (true) {
      case isMultiple(num, 15):
        return 'fizzBuzz';
      case isMultiple(num, 3):
        return 'Fizz';
      case isMultiple(num, 5):
        return 'Buzz';
      default:
        return num;
    }
  })
  .join(' ');

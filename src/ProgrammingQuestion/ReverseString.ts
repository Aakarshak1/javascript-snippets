let str: string = 'Hello';

// using built-in method
// split ==> reverse => join
let result_one = str.split('').reverse().join('');

// using for loop
let result_two: string = '';
for (let i = str.length - 1; i >= 0; i--) result_two += str[i];

//using split() and reduce()
let result_three = str.split('').reduce((reversed, character) => character + reversed, '');

// using recursion
function reverseString(str: string = ''): string {
  if (!str || typeof str !== 'string' || str.length < 2) return str;

  if (str === '') return '';
  else return reverseString(str.substr(1)) + str.charAt(0);
}
let result_four: string = reverseString(str);

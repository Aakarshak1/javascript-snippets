// prototypal chain
// what will be the output and why

/*
 *  output will be xyz

  ep is object is create from employee hence its employee wll be present on its prototype chain
  when we delete `cmp` from ep and try to console log ep.cmp
  since `ep` is linked to the `employee` object through the prototype chain. Therefore when `ep.comp` is accessed (called)
  it will follow the prototype chain and finds the `comp` property in the `employee` object. since the comp is present
  in  and inherited from the `employee` the output will be`xyz`

  even though the comp property is deleted from the ep object, it still exists in the prototype object
  (employee) due to the prototype chain. Hence, accessing ep.comp retrieves the value 'xyz' from the prototype
  object and logs it to the console.
 *
 */

var employee = {
  comp: 'xyz',
};

var ep = Object.create(employee);

delete ep.comp;
console.log(ep.comp);

/**
 * 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, ...
 */

//Using generator function

function* getFibonacciSequence() {
  let swap: number;
  let val1: number = 0;
  let val2: number = 1;

  yield val1;
  yield val2;

  while (true) {
    swap = val1 + val2;
    val1 = val2;
    val2 = swap;
    yield swap;
  }
}

let itr = getFibonacciSequence();
console.log(itr.next().value);
console.log(itr.next().value);

// calculating the next number by adding the current number to the old number.
function getFibonacciSequence_one(numTerms: number) {
  const sequence: number[] = [0, 1];

  for (let i = 2; i < numTerms; i++) {
    const nextTerm = sequence[i - 1] + sequence[i - 2];
    sequence.push(nextTerm);
  }

  return sequence;
}

const result = getFibonacciSequence_one(5);
console.log(result);

// using recursion
function getFibonacciSequence_two(num: number): number {
  if (num == 0) return 0;
  if (num == 1) return 1;
  return getFibonacciSequence_two(num - 2) + getFibonacciSequence_two(num - 1);
}

let numberTerms = 7;
for (let i = 0; i < numberTerms; i++) {
  console.log(getFibonacciSequence_two(i));
}

// Using Dynamic Programming ==> recursion + memoization/caching
/**
 * It uses a dynamic programming approach to store previously calculated Fibonacci numbers
 * in a cache called fibonacciCache. The function checks if the Fibonacci number for num is
 * already in the cache, and if so, returns it. Otherwise, it calculates the Fibonacci number
 * recursively by summing the previous two Fibonacci numbers and stores the result in the cache
 * before returning it.
 */

let fibonacciCache: number[] = [];
function getFibDynamic(num: number) {
  if (fibonacciCache[num] !== undefined) return fibonacciCache[num];
  if (num === 0) fibonacciCache[num] = 0;
  else if (num === 1 || num === 2) fibonacciCache[num] = 1;
  else fibonacciCache[num] = getFibDynamic(num - 1) + getFibDynamic(num - 2);
  return fibonacciCache[num];
}

for (let i = 0; i < numberTerms; i++) {
  console.log(getFibDynamic(i));
}

// using DP with no recursion

function getFibDynamic_two(num: number) {
  let fibonacciCache: number[] = [];
  for (let i = 0; i <= num; i++) {
    if (i === 0) fibonacciCache[i] = 0;
    else if (i <= 2) fibonacciCache[i] = 1;
    else fibonacciCache[i] = fibonacciCache[i - 1] + fibonacciCache[i - 2];
  }

  return fibonacciCache[num];
}

for (let i = 0; i < numberTerms; i++) {
  console.log(getFibDynamic_two(i));
}
